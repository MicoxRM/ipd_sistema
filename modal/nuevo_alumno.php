<div class="modal fade" id="nuevo_alumno" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
     <form action="" id="formulario">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Nuevo alumno</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
          
                 <div class="row">
             <div class="col-md-12">
                 <div class="input-group div-p">
  <div class="input-group-prepend">
    <span class="input-group-text" id=""><span class="fas fa-address-book"></span></span>
  </div>
  <input id="nombre" type="text" name="nombre" class="form-control" placeholder="Nombres " autocomplete="off">

</div>
             </div>
              <div class="col-md-12">
                 <div class="input-group div-p">
  <div class="input-group-prepend">
    <span class="input-group-text" id=""><span class="fas fa-user-circle"></span></span>
  </div>
  <input id="apellido" type="text" name="apellido" class="form-control" placeholder="Apellidos" autocomplete="off">

</div>
             </div>
             <div class="col-md-12">
                 <div class="input-group div-p">
  <div class="input-group-prepend">
    <span class="input-group-text" id=""><span class="fas fa-address-card"></span></span>
  </div>
  <input id="dni" type="text" name="dni" class="form-control" placeholder="Documento de identidad" autocomplete="off">

</div>
                 
             </div>
             <div class="col-md-12">
                 <div class="input-group div-p">
  <div class="input-group-prepend">
    <span class="input-group-text" id=""><span class="fas fa-phone"></span></span>
  </div>
  <input id="telefono" type="text" name="telefono" class="form-control" placeholder="Teléfono o Celular" autocomplete="off">

</div>
             </div>
             <div class="col-md-12">
                 <div class="input-group div-p">
  <div class="input-group-prepend">
    <span class="input-group-text" id=""><span class="fas fa-user"></span></span>
  </div>
  <input id="direccion" type="text" name="direccion" class="form-control" placeholder="Direccion" autocomplete="off">

</div>
                 
             </div>
                    <div class="col-md-12">
                 <div class="input-group div-p">
  <div class="input-group-prepend">
    <span class="input-group-text" id=""><span class="fas fa-envelope"></span></span>
  </div>
  <input id="email" type="email" name="email" class="form-control" placeholder="Correo Electronico" autocomplete="off">

</div>
                 
             </div>
                     <div class="col-md-12">
                 <div class="input-group div-p">
  <div class="input-group-prepend">
    <span class="input-group-text" id=""><span class="fas fa-map"></span></span>
  </div>
   <select name="distrito" id="distrito" class="form-control">
       
      <option value="" selected disabled>Escoja el distrito</option>
      <?php
      
      $sql = "SELECT * FROM distrito_actual";
                                        $resultado_distrito = $con->query($sql);

                                        while ($row_distrito = $resultado_distrito->fetch_assoc()){
      
      ?>
      <option value="<?php echo $row_distrito['id']; ?>"><?php echo $row_distrito["distrito"]; ?></option>
     
      
      <?php
                                        }
      ?>
      
  </select>
</div>
                 
             </div>
     
              <div class="col-md-12">
                 <div class="input-group div-p">
  <div class="input-group-prepend">
    <span class="input-group-text" id=""><span class="fas fa-child"></span></span>
  </div>
  <input id="nacimiento" type="date" name="nacimiento" class="form-control" value="<?php $x=date("Y-m-d"); echo $x; ?>"  autocomplete="off">

</div>
                 
             </div>
             <div class="col-md-12">
                 <div class="input-group div-p">
  <div class="input-group-prepend">
    <span class="input-group-text" id=""><span class="fas fa-mars"></span></span>
  </div>
  <select name="sexo" id="sexo" class="form-control">
       
      <option value="" selected disabled>Escoja el sexo del alumno</option>
      <option value="1">Masculino</option>
      <option value="2">Femenino</option>
      
      
  </select>
</div>
                 
             </div>
          </div>
     
             
             
             
          
             
        </div>
          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button id="guardar" type="button" class="btn btn-primary">Guardar</button>
      </div>
        </form> 
    </div>
  </div>
</div>