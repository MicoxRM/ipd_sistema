<div class="modal fade" id="nuevo_usuario" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
     <form action="" id="formulario">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Nuevo Usuario</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
          
                 <div class="row">
             <div class="col-md-12">
                 <div class="input-group div-p">
  <div class="input-group-prepend">
    <span class="input-group-text" id=""><span class="fas fa-address-book"></span></span>
  </div>
  <input id="nombre" type="text" name="nombre" class="form-control" placeholder="Nombres y apellidos" autocomplete="off">

</div>
             </div>
             <div class="col-md-12">
                 <div class="input-group div-p">
  <div class="input-group-prepend">
    <span class="input-group-text" id=""><span class="fas fa-address-card"></span></span>
  </div>
  <input id="dni" type="text" name="dni" class="form-control" placeholder="Documento de identidad" autocomplete="off">

</div>
                 
             </div>
             <div class="col-md-12">
                 <div class="input-group div-p">
  <div class="input-group-prepend">
    <span class="input-group-text" id=""><span class="fas fa-phone"></span></span>
  </div>
  <input id="telefono" type="text" name="telefono" class="form-control" placeholder="Teléfono o Celular" autocomplete="off">

</div>
             </div>
             <div class="col-md-12">
                 <div class="input-group div-p">
  <div class="input-group-prepend">
    <span class="input-group-text" id=""><span class="fas fa-user"></span></span>
  </div>
  <input id="user" type="text" name="user" class="form-control" placeholder="Usuario" autocomplete="off">

</div>
                 
             </div>
              <div class="col-md-12">
                 <div class="input-group div-p">
  <div class="input-group-prepend">
    <span class="input-group-text" id=""><span class="fas fa-lock"></span></span>
  </div>
  <input id="password" type="password" name="password" class="form-control" placeholder="Contraseña" autocomplete="off">

</div>
                 
             </div>
             <div class="col-md-12">
                 <div class="input-group div-p">
  <div class="input-group-prepend">
    <span class="input-group-text" id=""><span class="fas fa-toggle-on"></span></span>
  </div>
  <select name="tipo" id="tipo" class="form-control">
      
      <option value="" selected disabled>Escoja el tipo de usuario</option>
      <option value="1">Administrador</option>
      <option value="2">Secretaria</option>
      <option value="3">Portero</option>
      
  </select>
</div>
                 
             </div>
         </div>
     
             
             
             
          
             
        </div>
          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button id="guardar" type="button" class="btn btn-primary">Guardar</button>
      </div>
        </form> 
    </div>
  </div>
</div>