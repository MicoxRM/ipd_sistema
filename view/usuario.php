
<html lang="es">
<head>
   
   <title>IPD Tacna - Usuarios</title>
    
    <?php 
    
        include '../include/head.php';
    
    ?>
    <style>
    
        html,body{
            margin: 0px;
            padding: 0px;
            
        }
        .div-p{
            padding-top: 12px;
            padding-bottom: 12px;
        }
          .div-m{
            margin-top: 12px;
            margin-bottom: 12px;
        }
        
        .card-header{
            /*color: #E54949;
           */
        }
        
    </style>
      <script>
    
	$(document).on('ready',function(){

	  $('#guardar').click(function(){
          
      var nombre = $('#nombre').val();
 
 var dni = $('#dni').val();
  var telefono = $('#telefono').val();
  var user = $('#user').val();
  var password = $('#password').val();
 var tipo = $('#tipo').val();
if(nombre==='' || dni==='' || telefono==='' || user==='' || password==='' || tipo==='')
{
   
     $('#nuevo_usuario').modal('hide');
     
    $('#myModals').modal('show');
     
}
else
{     
   
		var url ="../ajax/nuevo_usuario.php";                                      

		$.ajax({                        
		   type: "POST",                 
		   url: url,                    
		   data: $("#formulario").serialize(),
		   success: function(data)            
		   {
                  $("#resp").addClass("");
               $("#hides").addClass("d-none");
			 $('#resp').html(data);   
            $("#formulario")[0].reset();
               
            $('#myModal').modal('show');
               $('#nuevo_usuario').modal('hide');
              $("#divid").load(" #divid");  
               
            /* var table = $('#table').DataTable();
 table.ajax.reload( null, false );*/
             //   var mytbl = $("#table").datatable();
 //mytbl.ajax.reload;
               
 
		   }
		 });
}
      }
    );
        
        
        
        
	});
	</script>

</head>
<body>
    <header>
      
          <?php
        
            include "../include/header.php";
        
        ?>
        
    </header> 
     
        <?php
          
        require "../include/navbar.php";
        require "../modal/nuevo_usuario.php";
     require "../modal/modal_confirmacion.php"
    
    
    ?> 
        
   
    
    
   <section class="container-fluid div-p">
 
       <div class="row">
           <div class="col-md-3">
               <div class="card div-m">
                   <div class="card-header b">
                       <span class="fa fa-wrench"></span> Operaciones
                   </div>
                    <div class="card-body">
                       <a type="button" class="btn btn-dark btn-block text-white" data-toggle="modal" data-target="#nuevo_usuario"><span class="fa fa-user"></span> Nuevo usuario</a>
                         <a type="button" class="btn btn-danger btn-block text-white"><span class="fa fa-file"></span> Generar reporte</a>
                   </div>
               </div>
                <div class="card div-m">
                   <div class="card-header b">
                        <span class="fa fa-history"></span> Cambios recientes 
                   </div>
                    <div class="card-body">
                     <p ><a   id="hides" class="">Ninguno por ahora</a></p>
                        <p ><a href="" id="resp"></a></p>
                   </div>
               </div>
           </div>
           <div class="col-md-6">
                <div class="card div-m">
                   <div class="card-header b">
                        <span class="fa fa-table"></span> Tabla de usuarios 
                   </div>
                    <div id="divid" class="card-body">
                     <div class="table-responsive-md">
  <table  class="table table-striped">
                           <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">NOMBRES</th>
      <th scope="col">APELLIDOS</th>
      <th scope="col">CARGO</th>
    </tr>
  </thead>
  <tbody>
   <?php
      $i=1;
         $consulta="SELECT * FROM usuario";
    $ejecutar = mysqli_query($con,$consulta);
          while($fila = mysqli_fetch_array($ejecutar)){
      ?>
    <tr>
      <th scope="row"><?php echo $i; ?></th>
      <td><?php echo $fila["usuario"]; ?></td>
      <td><?php echo $fila["dni"]; ?></td>
      <td><?php echo $fila["telefono"]; ?></td>
    </tr>
    <?php
        $i++;
          }
      
      ?>
  </tbody>
                          </table>
                        </div>

                   </div>
               </div>
           </div>
            <div class="col-md-3">
               <div class="card div-m">
                   <div class="card-header">
                       <h5 class="label"><span class="fa fa-envelope"></span> Mensajes</h5>
                   </div>
                    <div class="card-body">
                       
                   </div>
               </div>
                <div class="card div-m">
                   <div class="card-header">
                       <h5 class="label"><span class="fa fa-bell"></span> Alertas</h5>
                   </div>
                    <div class="card-body">
                       
                   </div>
               </div>
                <div class="card div-m">
                   <div class="card-header">
                       <h5 class="label"><span class="fa fa-book"></span> Notas</h5>
                   </div>
                    <div class="card-body">
                       
                   </div>
               </div>
           </div>
       </div>
       
   </section>
          <link rel="stylesheet" href="../css/dataTables.bootstrap4.min.css">
    <script src="../js/jquery.dataTable.min.js"></script>
    <script src="../js/dataTables.bootstrap4.min.js"></script>
    <footer>
        <?php 

            include '../include/footer.php';

        ?>

    </footer>
  
</body>
</html>