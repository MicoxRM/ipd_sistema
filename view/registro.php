
<html lang="es">
<head>
   
   <title>IPD Tacna - Registro</title>
    
    <?php 
    
        include '../include/head.php';
    
    ?>
    <style>
    
        html,body{
            margin: 0px;
            padding: 0px;
            
        }
        .div-p{
            padding-top: 12px;
            padding-bottom: 12px;
        }
          .div-m{
            margin-top: 12px;
            margin-bottom: 12px;
        }
    </style>
   <script>
    
	$(document).on('ready',function(){

	  $('#guardar').click(function(){
          
      var nombre = $('#nombre').val();
  var apellido = $('#apellido').val();
 var dni = $('#dni').val();
  var telefono = $('#telefono').val();
  var direccion = $('#direccion').val();
  var distrito = $('#distrito').val();
 var sexo = $('#sexo').val();
 var email = $('#email').val();         
 var nacimiento = $('#nacimiento').val();
          
if(nombre==='' || dni==='' || telefono==='' || direccion==='' || distrito==='' || sexo==='' || nacimiento==='')
{
   
     $('#nuevo_alumno').modal('hide');
     
    $('#myModals').modal('show');
     
}
else
{     
   
		var url ="../ajax/nuevo_alumno.php";                                      

		$.ajax({                        
		   type: "POST",                 
		   url: url,                    
		   data: $("#formulario").serialize(),
		   success: function(data)            
		   {
                  $("#resp").addClass("");
               $("#hides").addClass("d-none");
			 $('#resp').html(data);   
            $("#formulario")[0].reset();
               
            $('#myModal').modal('show');
               $('#nuevo_alumno').modal('hide');
             
               window.setTimeout('location.href="registro.php"', 2000);  
            /* var table = $('#table').DataTable();
 table.ajax.reload( null, false );*/
             //   var mytbl = $("#table").datatable();
 //mytbl.ajax.reload;
               
 
		   }
		 });
}
      }
    );
        
        
        
        
	});
	</script>

     
          
                    
</head>

<body id="body">
    <header>
       <div class="row">
           
               <div class="col-md-12 bg-light">
         
          <div class="container-fluid div-p">
                      <img src="../img/logo_ipd.png" width="220" alt="">
  
              
          </div>
         </div>
       </div> 
        
        
    </header> 
     
        <?php
          
        require "../include/navbar.php";
         require "../modal/nuevo_alumno.php";
     require "../modal/modal_confirmacion_recarga.php"
    ?> 
        
   
    
    
   <section class="container-fluid div-p">
 
       <div class="row">
             <div class="col-md-3">
               <div class="card div-m">
                   <div class="card-header">
                      <h5 class="label"><span class="fa fa-wrench"></span> Operaciones</h5>
                   </div>
                    <div class="card-body">
                       <a type="button" class="btn btn-dark btn-block text-white" data-toggle="modal" data-target="#nuevo_alumno"><span class="fa fa-user"></span> Nuevo alumno</a>
                         <a type="button" class="btn btn-danger btn-block text-white"><span class="fa fa-file"></span> Generar reporte</a>
                   </div>
               </div>
               <div class="card div-m">
                   <div class="card-header">
                       <h5 class="label"><span class="fa fa-envelope"></span> Mensajes</h5>
                   </div>
                    <div class="card-body">
                       
                   </div>
               </div>
                <div class="card div-m">
                   <div class="card-header">
                       <h5 class="label"><span class="fa fa-bell"></span> Alertas</h5>
                   </div>
                    <div class="card-body">
                       
                   </div>
               </div>
                <div class="card div-m">
                   <div class="card-header">
                       <h5 class="label"><span class="fa fa-book"></span> Notas</h5>
                   </div>
                    <div class="card-body">
                       
                   </div>
               </div>
           </div>
            <div class="col-md-9">
                <div class="card div-m">
                   <div class="card-header">
                       <h5 class="label"><span class="fa fa-desktop"></span> Panel de registro de alumnos</h5>
                   </div>
                    <div class="card-body">
                       <div class="container">
                       <table id="table" class="table table-bordered">
                           <thead>
                               <tr>
                                   <td>
                                       DNI
                                   </td>
                                   <td>
                                       ALUMNO
                                   </td>
                                   <td>
                                       DIRECCION
                                   </td>
                                   <td>
                                       TELEFONO
                                   </td>
                               </tr>
                           </thead>
                           <tbody>
                               
                                  <?php 
                                        $sql = "SELECT * FROM estudiante";
                                        $resultado = $con->query($sql);

                                        while ($row = $resultado->fetch_assoc()){
                                        ?>
                                        <tr>
                                            <td>
                                                <?php echo $row["dni"];  ?>
                                            </td>
                                            <td>
                                                 <?php echo $row["nombres"]." ".$row["apellidoss"];  ?>
                                            </td>
                                            <td>
                                                 <?php echo $row["direccion"];  ?>
                                            </td>
                                            <td>
                                                 <?php echo $row["telefono"];  ?>
                                            </td>
                                            
                                        </tr>
                                         
        
                                        
                                        
                                        <?php
                                        }
                           ?>
                               
                               
                               
                           </tbody>
                           
                       </table>
     
                           
    
    </div> 

                   </div>
               </div>
           </div>
           
       </div>
       
   </section>
       <link rel="stylesheet" href="../css/dataTables.bootstrap4.min.css">
    <script src="../js/jquery.dataTable.min.js"></script>
    <script src="../js/dataTables.bootstrap4.min.js"></script> 
    <footer>
        <?php 

            include '../include/footer.php';

        ?>

    </footer>
   
</body>
</html>