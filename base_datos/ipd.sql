-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 22-01-2019 a las 04:36:07
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ipd`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departemento_proc`
--

CREATE TABLE `departemento_proc` (
  `id` int(11) NOT NULL,
  `departamento_proc` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `departemento_proc`
--

INSERT INTO `departemento_proc` (`id`, `departamento_proc`) VALUES
(1, 'TACNA'),
(2, 'PUNO'),
(3, 'MOQUEGUA'),
(4, 'CUSCO'),
(5, 'LIMA'),
(6, 'AREQUIPA'),
(7, 'OTRO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `distrito_actual`
--

CREATE TABLE `distrito_actual` (
  `id` int(11) NOT NULL,
  `distrito` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `distrito_actual`
--

INSERT INTO `distrito_actual` (`id`, `distrito`) VALUES
(0, 'TACNA'),
(1, 'TACNA'),
(2, 'GREGORIO ALBARRACIN'),
(3, 'CIUDAD NUEVA'),
(4, 'POCOLLAY'),
(5, 'LA YARADA LOS PALOS'),
(6, 'ALTO DE LA ALIANZA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estudiante`
--

CREATE TABLE `estudiante` (
  `id` int(11) NOT NULL,
  `dni` char(8) DEFAULT NULL,
  `codigo_barras` int(11) DEFAULT NULL,
  `nombres` varchar(100) DEFAULT NULL,
  `apellidoss` varchar(100) DEFAULT NULL,
  `direccion` varchar(150) DEFAULT NULL,
  `nacimiento` date DEFAULT NULL,
  `sexo` varchar(15) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `telefono` varchar(50) DEFAULT NULL,
  `distrito_actual_id` int(11) NOT NULL,
  `departemento_proc_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `estudiante`
--

INSERT INTO `estudiante` (`id`, `dni`, `codigo_barras`, `nombres`, `apellidoss`, `direccion`, `nacimiento`, `sexo`, `email`, `telefono`, `distrito_actual_id`, `departemento_proc_id`) VALUES
(1, '71247189', 123456789, 'ABEL', 'MAQUERA', 'LEGUIA MZ.35 LT.23', '1994-10-20', 'MASCULINO', 'jhanmaquera@gmail.com', '945642283', 1, 1),
(2, '00485629', 135792468, 'NICOLAS', 'MAQUERA MARAS', 'LEGUIA MZ.B LT.2', '1972-06-16', 'MASCUILINO', 'madnick_10@hotmail.com', '952285631', 1, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `departemento_proc`
--
ALTER TABLE `departemento_proc`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `distrito_actual`
--
ALTER TABLE `distrito_actual`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `estudiante`
--
ALTER TABLE `estudiante`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_estudiante_distrito_actual_idx` (`distrito_actual_id`),
  ADD KEY `fk_estudiante_departemento_proc1_idx` (`departemento_proc_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `estudiante`
--
ALTER TABLE `estudiante`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `estudiante`
--
ALTER TABLE `estudiante`
  ADD CONSTRAINT `fk_estudiante_departemento_proc1` FOREIGN KEY (`departemento_proc_id`) REFERENCES `departemento_proc` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_estudiante_distrito_actual` FOREIGN KEY (`distrito_actual_id`) REFERENCES `distrito_actual` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
