<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mostrar codigo de barras</title>
    <?php 
    
        include '../include/head.php';
    
    ?>
</head>
<body>
    <div class="container">
        <h1>Generar código de barras:</h1>
        <div class="row">
            <div class="col-sm-4">
                <form action="../php/insertar.php" method="post">
                    <label for="">Nombre</label>
                    <input type="text" name="codigo" class="form-control">
                    <br>
                    <button type="submit" class="btn btn-primary">Generar código</button>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-10">
                <br>
                <?php require_once "tabla.php"; ?>
            </div>
        </div>
    </div>
</body>
</html>

